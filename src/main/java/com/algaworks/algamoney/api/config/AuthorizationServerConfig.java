package com.algaworks.algamoney.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 17/08/17.
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private static final String WRITE = "write";
    private static final String READ = "read";
    private static final String CLIENT_ID = "angular";
    private static final String SIGNING_KEY = "algaworks";
    public static final int TOKEN_VALIDITY_SECONDS = 20;
    public static final int REFRESH_TOKEN_VALIDITY_SECONDS = 3600 * 24;
    public static final String CLIENT_PWD = "@ngul@r0";
    public static final String GRANT_TYPE_PWD = "password";
    public static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(CLIENT_ID)
                .secret(CLIENT_PWD)
                .scopes(READ, WRITE)
                .authorizedGrantTypes(GRANT_TYPE_PWD, GRANT_TYPE_REFRESH_TOKEN)
                .accessTokenValiditySeconds(TOKEN_VALIDITY_SECONDS)
                .refreshTokenValiditySeconds(REFRESH_TOKEN_VALIDITY_SECONDS);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore())
                .accessTokenConverter(accessTokenConverter())
                .reuseRefreshTokens(false)
                .authenticationManager(authenticationManager);
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter acessTokenConverter = new JwtAccessTokenConverter();
        acessTokenConverter.setSigningKey(SIGNING_KEY);
        return acessTokenConverter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }
}
