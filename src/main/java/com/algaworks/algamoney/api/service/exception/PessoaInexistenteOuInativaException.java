package com.algaworks.algamoney.api.service.exception;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 16/08/17.
 */
public class PessoaInexistenteOuInativaException extends Exception {
}
