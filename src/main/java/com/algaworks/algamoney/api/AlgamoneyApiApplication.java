package com.algaworks.algamoney.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/07/17.
 */
@SpringBootApplication
public class AlgamoneyApiApplication {

    public static void main(String... args) {
        SpringApplication.run(AlgamoneyApiApplication.class, args);
    }
}
