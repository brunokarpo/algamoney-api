package com.algaworks.algamoney.api.model;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 11/08/17.
 */
public enum TipoLancamento {

    RECEITA,
    DESPESA;
}
