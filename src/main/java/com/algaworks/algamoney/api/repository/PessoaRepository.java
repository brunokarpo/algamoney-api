package com.algaworks.algamoney.api.repository;

import com.algaworks.algamoney.api.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/08/17.
 */
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
}
