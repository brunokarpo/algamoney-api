package com.algaworks.algamoney.api.repository;

import com.algaworks.algamoney.api.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 02/08/17.
 */
public interface CategoriaRepository extends JpaRepository<Categoria, Long>{
}
